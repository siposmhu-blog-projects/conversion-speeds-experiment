# conversion-speeds-experiment

Creating test cases and measuring how much each string to int conversion method takes. Display these data on a graph.

Read my relevant blog post to better understand: https://siposm.hu/blog/testing-string-to-int-conversion-speeds